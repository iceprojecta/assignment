import { Component, OnInit } from '@angular/core';
import {LastGameServiceService} from '../last-game-service.service';
@Component({
  selector: 'app-last-game',
  templateUrl: './last-game.component.html',
  styleUrls: ['./last-game.component.css']
})
export class LastGameComponent implements OnInit {
title = "Last Game";
lastgame;
points =[];
results;

  constructor(private lastgameService: LastGameServiceService ) { }

  ngOnInit() {
    this.lastgameService.getLastGame().subscribe((data) => {
      console.log(data);
      this.lastgame = data;
      this.lastgame = Array.of(this.lastgame);
      this.lastgame.forEach(e => {
      this.lastgame = e.table;
      });
   
    var count = 0;

    this.results.forEach(e =>{
    this.lastgame.length = count + 1;
    this.lastgame[count] = e;
    count++;
    });
 });
 
}


}
