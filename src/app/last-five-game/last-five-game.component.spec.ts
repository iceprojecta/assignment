import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LastFiveGameComponent } from './last-five-game.component';

describe('LastFiveGameComponent', () => {
  let component: LastFiveGameComponent;
  let fixture: ComponentFixture<LastFiveGameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LastFiveGameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LastFiveGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
