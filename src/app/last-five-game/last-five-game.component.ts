import { Component, OnInit } from '@angular/core';
import {LastFiveGamesService} from '../last-five-games.service';

@Component({
  selector: 'app-last-five-game',
  templateUrl: './last-five-game.component.html',
  styleUrls: ['./last-five-game.component.css']
})
export class LastFiveGameComponent implements OnInit {
title = "Last Five Game";
lastfivegame;

  constructor(private lastfivegameService: LastFiveGamesService ) { }

  ngOnInit() {
  this.lastfivegameService.getLastFiveGame().subscribe((data) => {
  console.log(data);
  this.lastfivegame = data;
  this.lastfivegame = Array.of(this.lastfivegame);
  });

  }
}
