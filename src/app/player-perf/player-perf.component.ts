import { Component, OnInit } from '@angular/core';
import { PlayerPerfService } from '../player-perf.service'

@Component({
  selector: 'app-player-perf',
  templateUrl: './player-perf.component.html',
  styleUrls: ['./player-perf.component.css']
})
export class PlayerPerfComponent implements OnInit {
  title = "Player Performance";
  playerPerf;
  squad;
  goals = [];

  constructor(private playerPerformance: PlayerPerfService) { }

  ngOnInit() {
    this.getPlayerPerf();
    this.getScorers();
  }

  getPlayerPerf() {
    this.playerPerformance.getPlayerPerformance().subscribe((data) => {
      console.log(data);
      //cannot bind the squad data directly from json obj
      this.squad = data;
      this.squad = this.squad.squad;
      console.log(this.squad);
    });
  }

  getScorers() {
    this.playerPerformance.getScorers().subscribe((data) => {
      //cannot bind the scorer data directly from json obj
      this.playerPerf = data;
      var temp = this.playerPerf.scorers;
      var count = 0;
      var pos = 0;
      //count how many goals scored from our team
      temp.forEach(e => {
        if(e.team.name == "FC Barcelona") {
          count++;
        }
      });
      //set array length
      this.goals.length = count;
      //populate array
      for(var i = 0; i < temp.length; i++) {
        if(temp[i].team.name == "FC Barcelona") {
          console.log(temp[i]);
          this.goals[pos] = temp[i];
          pos++;
        }
      }
      console.log(this.goals);
    });
  }

  private addScoresToSquad(currPlayer: any) {
    if(currPlayer.role == "COACH") {
      return null;
    }
    for(var i = 0; i < this.goals.length; i++) {
      if(this.goals[i].player.name == currPlayer.name) {
        return this.goals[i].numberOfGoals;
      }
    }
    return '-';
  }

}
