import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerPerfComponent } from './player-perf.component';

describe('PlayerPerfComponent', () => {
  let component: PlayerPerfComponent;
  let fixture: ComponentFixture<PlayerPerfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerPerfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerPerfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
