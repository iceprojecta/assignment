import { Component, OnInit } from '@angular/core';
import { PointsService } from '../points.service';

@Component({
  selector: 'app-points',
  templateUrl: './points.component.html',
  styleUrls: ['./points.component.css']
})
export class PointsComponent implements OnInit {
  title = "Points table for La Liga";
  season;
  points = [];
  standings;

  constructor(private pointsService: PointsService) { }

  ngOnInit() {
    this.getPoints();
  }

  getPoints() {
    this.pointsService.getAllPoints().subscribe((data) => {
      this.season = data;
      this.season = this.season.standings[0];
      this.season = Array.of(this.season);
      this.season.forEach(e => {
        this.standings = e.table;
      });

      var count = 0;
      this.standings.forEach(e => {
        this.points.length = count + 1;
        this.points[count] = e;
        count++;
      });
    });
  }

}
