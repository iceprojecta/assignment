import { Component, OnInit } from '@angular/core';
import {H2hMatchResRivalService} from '../h2h-match-res-rival.service';

@Component({
  selector: 'app-h2h-match-res-rival',
  templateUrl: './h2h-match-res-rival.component.html',
  styleUrls: ['./h2h-match-res-rival.component.css']
})
export class H2hMatchResRivalComponent implements OnInit {
  title = "H2H Match Results with Rivals";
  h2hmatchresrival;
  matches;
  res = [];

  constructor(private h2hmatchresrivalService: H2hMatchResRivalService ) { }

  ngOnInit() {
    this.h2hmatchresrivalService.getH2hMatchResRival().subscribe((data) => {
      console.log(data);
      this.h2hmatchresrival = data;
      this.h2hmatchresrival = Array.of(this.h2hmatchresrival);
      this.h2hmatchresrival.forEach(e => {
        this.matches = e.matches;
      });

      var count = 0;
      this.matches.forEach(e => {
        this.res.length = count + 1;
        this.res[count] = e;
        count++;
      });
    });
  }

  private formatWinner(result: any) {
    if(result == "HOME_TEAM") {
      return "HOME";
    } else if(result == "AWAY_TEAM") {
      return "AWAY";
    } else {
      return "DRAW";
    }
  }

}
