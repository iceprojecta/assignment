import { Component, OnInit } from '@angular/core';
import { TopGoalScorersService } from "../top-goal-scorers.service";

@Component({
  selector: 'app-top-goal-scorers',
  templateUrl: './top-goal-scorers.component.html',
  styleUrls: ['./top-goal-scorers.component.css']
})
export class TopGoalScorersComponent implements OnInit {
  title = "Top 5 goal scorers";
  topGoal;
  goals = [];

  constructor(private topGoalService: TopGoalScorersService) { }

  ngOnInit() {
    this.getScorers();
  }

  getScorers() {
    this.topGoalService.getScorers().subscribe((data) => {
      //cannot bind the scorer data directly from json obj
      this.topGoal = data;
      var temp = this.topGoal.scorers;
      var count = 0;
      var pos = 0;
      //set array length
      this.goals.length = 5;
      //populate array, limit to 5 for top 5 of our team
      var index = 0;
      while(pos < 5) {
        if(temp[index].team.name == "FC Barcelona") {
          this.goals[pos] = temp[index];
          pos++;
        }
        index++;
      }
    });
  }

}
