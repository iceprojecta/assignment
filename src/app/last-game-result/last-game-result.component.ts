import { Component, OnInit } from '@angular/core';
import {LastGameResultService} from '../last-game-result.service';

@Component({
  selector: 'app-last-game-result',
  templateUrl: './last-game-result.component.html',
  styleUrls: ['./last-game-result.component.css']
})
export class LastGameResultComponent implements OnInit {
title = "Last Game Result";
lastgameresult;

  constructor(private lastgameresultService: LastGameResultService ) { }

  ngOnInit() {
  this.lastgameresultService.getLastGameResult().subscribe((data) => {
  console.log(data);
  this.lastgameresult = data;
  this.lastgameresult = Array.of(this.lastgameresult);
  });

  }

}
