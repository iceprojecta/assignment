import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LastGameResultService {
private API_KEY = '066cc2675dde4a13b9d8c6daf973ab83';
  private URL = 'https://api.football-data.org/v2/';

  private httpOptions = {
    headers: new HttpHeaders({
      'X-Auth-Token': this.API_KEY
    })
  };

  constructor(private http: HttpClient) { }

  private extractData(res: Response) {
  	const body = res;
  	return body || {};
  }

  public getLastGameResult() {
    return this.http.get(this.URL + 'teams/competitions/81/matches?lastgame=FINISHED', this.httpOptions).pipe(
    map(this.extractData));
  }

}
