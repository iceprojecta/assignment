import { Component, OnInit } from '@angular/core';
import {AllGameResService} from '../all-game-res.service';

@Component({
  selector: 'app-all-game-res',
  templateUrl: './all-game-res.component.html',
  styleUrls: ['./all-game-res.component.css']
})
export class AllGameResComponent implements OnInit {

  title = "All Game Result";
  allgameres;
  restemp;
  res = [];

    constructor(private allgameresService: AllGameResService ) { }

    ngOnInit() {
      this.initAllGames();
    }

    initAllGames() {
      this.allgameresService.getAllGameRes().subscribe((data) => {
        console.log(data);
        this.allgameres = data;
        this.allgameres = Array.of(this.allgameres);
        this.allgameres.forEach(e => {
          this.restemp = e.matches;
        });

        var count = 0;
        this.restemp.forEach(e => {
          this.res.length = count + 1;
          this.res[count] = e;
          count++;
        });
      });
    }

    private formatWinner(result: any) {
      if(result == "HOME_TEAM") {
        return "HOME";
      } else if(result == "AWAY_TEAM") {
        return "AWAY";
      } else {
        return "DRAW";
      }
    }
}
