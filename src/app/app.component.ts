import { Component } from '@angular/core';
import { MainApiService } from './main-api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'La Liga Santander League';
  footer = "Created by Steven Zisis (19698658), Naeim Ismeth (19612585) and Atiq Shahriar Rafi (19220514)";
  crest;

  constructor(private api: MainApiService) { }

  ngOnInit() {
    this.getCrest();
  }

  getCrest() {
    this.api.getCrest().subscribe((data) => {
      this.crest = data;
      this.crest = Array.of(this.crest);
    });
  }
}
